package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type City struct {
	ID    int    `db:"id"`
	Name  string `db:"name"`
	State string `db:"state"`
}

func main() {
	db, err := sqlx.Connect("postgres", "user=postgres password=yourpassword dbname=test sslmode=disable")
	if err != nil {
		fmt.Println("Error connecting to DB:", err)
		return
	}

	cityRepo := &CityRepository{db: db}

	newCity := &City{
		ID:    1,
		Name:  "MSKW",
		State: "123",
	}

	err = cityRepo.Create(newCity)
	if err != nil {
		fmt.Println("Error creating city:", err)
		return
	}

	cities, err := cityRepo.List()
	if err != nil {
		fmt.Println("Error listing cities:", err)
		return
	}
	fmt.Println("Cities:", cities)

	newCity.Name = "New York City"
	err = cityRepo.Update(newCity)
	if err != nil {
		fmt.Println("Error updating city:", err)
		return
	}

	err = cityRepo.Delete(newCity.ID)
	if err != nil {
		fmt.Println("Error deleting city:", err)
		return
	}
}

type CityRepository struct {
	db *sqlx.DB
}

func (r *CityRepository) Create(city *City) error {
	query := `INSERT INTO cities (id, name, state) VALUES ($1, $2, $3)`
	_, err := r.db.Exec(query, city.ID, city.Name, city.State)
	return err
}

func (r *CityRepository) Delete(id int) error {
	query := `DELETE FROM cities WHERE id = $1`
	_, err := r.db.Exec(query, id)
	return err
}

func (r *CityRepository) Update(city *City) error {
	query := `UPDATE cities SET name = $1, state = $2 WHERE id = $3`
	_, err := r.db.Exec(query, city.Name, city.State, city.ID)
	return err
}

func (r *CityRepository) List() ([]City, error) {
	var cities []City
	query := `SELECT * FROM cities`
	err := r.db.Select(&cities, query)
	return cities, err
}
